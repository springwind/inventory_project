using static UnityEditor.Progress;
using UnityEngine;
using UnityEngine.UI;

public class ObjectItem : MonoBehaviour, IObjectItem
{
    [Header("아이템")]
    public ScriptableObeject_MyOwn item;
    [Header("아이템 이미지")]
    public Image itemImage;

    /// <summary>
    /// 스크립터블 오브젝트 아이템의 이미지를 스프라이트 이미지로 저장
    /// </summary>
    void Start()
    {
        itemImage.sprite = item.itemImage;
    }

    /// <summary>
    /// 아이템 클릭시 반환하는 메서드
    /// </summary>
    /// <returns></returns>
    public ScriptableObeject_MyOwn ClickItem()
    {
        return this.item;
    }
}