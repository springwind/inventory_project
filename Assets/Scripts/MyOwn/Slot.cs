using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Slot : MonoBehaviour
{
    /// <summary>
    /// 이미지 컴포넌트를 담을곳
    /// </summary>
    [SerializeField] Image image;

    /// <summary>
    /// 스크립터블 오브젝트 객체 받아오는곳
    /// </summary>
    private ScriptableObeject_MyOwn _item;

    
    public ScriptableObeject_MyOwn item
    {
        // 슬롯 내부에 있는 아이템의 정보를 넘겨줌
        get { return _item; }
        set
        {
            // 아이템에 들어온정보를 _item 에 전달
            _item = value;

            // Inventory.cs 의 리스트 items 에 등록된 아이템있다면
            if (_item != null)
            {
                // itemImage 를 image에 저장
                image.sprite = item.itemImage;
                // 알파값을 1로하여 아이템 표기
                image.color = new Color(1, 1, 1, 1);
            }
            else
            {
                // 아니라면 알파값을 0으로하여 아이템 비표기
                image.color = new Color(1, 1, 1, 0);
            }
        }
    }
}