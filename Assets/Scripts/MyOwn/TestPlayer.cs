using JetBrains.Annotations;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 아이템 추가 스크립트
/// </summary>
public class TestPlayer : MonoBehaviour
{
    [Header("인벤토리")]
    public Inventory inventory;

    void Update()
    {
        // 마우스 좌클릭이 입력됐을시 레이케스트를 이용하여 박스 콜라이더 감지시 ItemAdd 메서드 호출
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);

            if (hit.collider != null)
            {
                ItemAdd(hit);
            }
        }
    }

    
    void ItemAdd(RaycastHit2D hit)
    {
        // 클릭된 오브젝트 IObjectItem 인터페이스를 clickInterface에 넘겨줌
        IObjectItem clickInterface = hit.transform.gameObject.GetComponent<IObjectItem>();

        // clickInterface 가 값을 가지고 있다면
        if (clickInterface != null)
        {
            // item 에 클릭된 오브젝트 아이템 정보 넘겨줌
            ScriptableObeject_MyOwn item = clickInterface.ClickItem();
            print($"{item.itemName}");
            inventory.AddItem(item);
        }
    }
    

}
