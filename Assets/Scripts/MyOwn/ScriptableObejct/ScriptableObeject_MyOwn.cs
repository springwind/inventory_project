using UnityEngine;

/// <summary>
/// 아이템 정보를 담을 스크립터블 오브젝트
/// </summary>
[CreateAssetMenu]
public class ScriptableObeject_MyOwn : ScriptableObject
{
    /// <summary>
    /// 아이템이름
    /// </summary>
    public string itemName;
    /// <summary>
    /// 아이템 이미지
    /// </summary>
    public Sprite itemImage;
}
