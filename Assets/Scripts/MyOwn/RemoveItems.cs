using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 아이템 제거 스크립트
/// </summary>
public class RemoveItems : MonoBehaviour
{

    public Inventory inventory;
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero);

            if (hit.collider != null)
            {
                RemovingItem(hit);
            }
            
        }
    }

    void RemovingItem(RaycastHit2D hit)
    {
        IObjectItem clickInterface = hit.transform.gameObject.GetComponent<IObjectItem>();

        if (clickInterface != null)
        {
            ScriptableObeject_MyOwn item = clickInterface.ClickItem();
            print($"{item.itemName}");
            inventory.RemoveItem(item);
        }
    }

   
}
