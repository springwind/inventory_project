using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 아이템 인벤토리를 관리하는 클래스
/// </summary>
public class Inventory : MonoBehaviour
{
    /// <summary>
    /// 아이템 담을 리스트
    /// </summary>
    public List<ScriptableObeject_MyOwn> items;

    /// <summary>
    /// 슬롯들의 부모를 담을곳
    /// </summary>
    [SerializeField]
    private Transform slotParent;

    /// <summary>
    /// 위의 하위의 것들인 slot을 다믕ㄹ곳
    /// </summary>
    [SerializeField]
    private Slot[] slots;

#if UNITY_EDITOR
    /// <summary>
    /// Slot 스크립트가 적용된 컴포넌트들을 자동 추가
    /// </summary>
    private void OnValidate()
    {
        slots = slotParent.GetComponentsInChildren<Slot>();
    }
#endif

    /// <summary>
    /// 게임 시작시 items에 들어있는 아이템을 인벤토리에 넣음
    /// </summary>
    void Awake()
    {
        FreshSlot();       
    }

    /// <summary>
    /// 아이템이 추가되거나 제거될 떄 slot의 내용을 재정렬
    /// </summary>
    public void FreshSlot()
    {
        int i = 0;
        for (; i < items.Count && i < slots.Length; i++)
        {
            slots[i].item = items[i];
        }
        for (; i < slots.Length; i++)
        {
            slots[i].item = null;
        }
    }

    /// <summary>
    /// 아이템 획득 클래스
    /// </summary>
    /// <param name="_item"></param>
    public void AddItem(ScriptableObeject_MyOwn _item)
    {
        if (items.Count < slots.Length)
        {
            items.Add(_item);
            FreshSlot();
        }
        else
        {
            print("슬롯이 가득 차 있습니다.");
        }
    }
    /// <summary>
    /// 아이템 제거 클래스
    /// </summary>
    /// <param name="_item"></param>
    public void RemoveItem(ScriptableObeject_MyOwn _item)
    {
        
        items.Remove(_item);
        FreshSlot();
    }

    
}